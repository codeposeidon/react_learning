import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import One from './components/one.component';

 
function App() {
  return (
        <section class="container text-center overflow-hidden">
            <One/>,
        </section>
  );
}

export default App;
