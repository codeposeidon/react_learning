import React from 'react';
import image1 from '../images/pic02.jpg';
import image2 from '../images/pic03.jpg';
import image3 from '../images/pic01.jpg';
import image4 from '../images/pic04.jpg';
import image5 from '../images/pic05.jpg';
import image6 from '../images/pic06.jpg';
import image7 from '../images/pic07.jpg';
import image8 from '../images/pic08.jpg';

function One() { 
    return ( 
        <div className="container m-0 mt-5 p-0 shadow">
            <div className="row align-items-start text-center text-light pt-5">
                <div className="col">
                    <h1 className="txt m-0 p-0">Caminar</h1>
                    <p className="mt-0 pt-0 txt2">By TEMPLATED</p>
                    <img src={image1} alt="girl" className="w-100"/>
                </div>
            </div>

            <div className="row align-items-start m-0 p-0 h-300 bg-light text-dark">
                <div className="col">
                    <h2 className="p-1 pt-5 txt3"> Fringilla Fermentum Tellus</h2>
                    <hr className="line"></hr>
                   <p className="txt4">VEHICULA URNA SED JUSTO BIBENDUM</p> 
                </div>
            </div>

            <div className="row m-0 p-0 block3">
                <div className="col p-20">
                  <p className="text-left p-5">Curabitur eget semper ante. Morbi eleifend ultricies est, a blandit diam
                    vehicula vel. Vestibulum porttitor nisi quis viverra hendrerit. Suspendisse
                    vel volutpat nibh, vel elementum lacus. Maecenas commodo pulvinar dui, at
                    cursus metus imperdiet vel. Vestibulum et efficitur urna. Duis velit nulla,
                    interdum sed felis sit amet, facilisis auctor nunc. Cras luctus urna at risus
                    feugiat scelerisque nec sed diam.<br/><br/>
                    Nunc fringilla metus odio, at rutrum augue tristique vel. Nulla ac tellus a neque ullamcorper
                    porta a vitae ipsum. Morbi est sapien, hendrerit quis mi in, aliquam bibendum orci. Vestibulum
                    imperdiet nibh vitae maximus posuere. Aenean sit amet nibh feugiat, condimentum tellus eu,
                    malesuada nunc. Mauris ac pulvinar turpis, sit amet pharetra est. Ut bibendum justo condimentum,
                    vehicula ex vel, venenatis libero. Etiam vehicula urna sed justo bibendum, ac lacinia nunc pulvinar.
                    Integer nec velit orci. Vestibulum pellentesque varius dapibus. Morbi ullamcorper augue est, sed luctus
                    orci fermentum dictum. Nunc tincidunt, nisl consequat convallis viverra, metus nisl ultricies dui,
                    vitae dapibus ligula urna sit amet nibh. Duis ut venenatis enim.
                  </p>
                </div>
            </div>

             <div className="row m-0 h-auto block4">
                <div className="container m-5 p-0">
                   <div className="row p-0 m-0">
                       <div className="col text-white txt5">
                          <h2 className="pt-3">Vestibulum Convallis</h2>
                          <hr className="line border-white"></hr>
                          <p className="text-secondary pt-1">VEHICULA URNA SED JUSTO BIBENDUM</p>
                       </div>
                   </div>
                   <div className="row p-0 m-0">
                       <div className="col-md-6 p-0"><img className="w-100 h-auto" id="cat" src={image2} /></div>
                       <div className="col-md-6 p-0"><img className="w-100 h-auto" src={image3}/></div>    
                    </div>
                    <div className="row p-0 m-0">            
                       <div className="col-md-6 p-0"><img className="w-100 h-auto" src={image4}/></div>
                       <div className="col-md-6 p-0"><img className="w-100 h-auto" src={image5}/></div>
                   </div>
                </div>
            </div> 

            <div className="row m-0 h-auto block4">
                   <div className="row m-0">
                       <div className="col-lg-6 p-0"><img className="w-100 h-auto" src={image6}/></div>
                       <div className="col-lg-6 p-0 bg-light text-dark">
                           <table className="h-100 w-100">
                              <tbody>
                                <tr>
                                    <td className="align-middle text-left p-5">
                                            <h2>Viverra Hendrerit</h2>
                                            <p>Curabitur eget semper ante. Morbi eleifend ultricies est, a blandit diam vehicula vel.
                                               Vestibulum porttitor nisi quis viverra hendrerit. Suspendisse vel volutpat nibh, vel elementum 
                                               lacus. Maecenas commodo pulvinar dui, at cursus metus imperdiet vel.
                                            </p>
                                    </td>
                                </tr>
                              </tbody>
                            </table>
                        </div>  
                    </div>    
                    <div className="row rv_row m-0">          
                       <div className="col-lg-6 p-0 text-light">
                            <table className="h-100 w-100">
                                    <tbody>
                                        <tr>
                                            <td className="align-middle text-right p-5">
                                                    <h2>Curabitur Eget</h2>
                                                    <p> Curabitur eget semper ante. Morbi eleifend ultricies est, a blandit diam vehicula vel.
                                                        Vestibulum porttitor nisi quis viverra hendrerit. Suspendisse vel volutpat nibh, vel 
                                                        elementum lacus. Maecenas commodo pulvinar dui, at cursus metus imperdiet vel.
                                                    </p>
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                       </div>
                       <div className="col-lg-6 p-0"><img className="w-100 h-auto" src={image7}/></div>
                    </div>
                    <div className="row m-0">
                       <div className="col-lg-6 p-0"><img className="w-100 h-auto" src={image8}/></div>
                       <div className="col-lg-6 p-0">
                            <table className="h-100 w-100 bg-white text-dark">
                                    <tbody>
                                        <tr>
                                            <td className="align-middle text-left p-5">
                                                    <h2>Morbi Eleifend</h2>
                                                    <p> Curabitur eget semper ante. Morbi eleifend ultricies est, a blandit diam vehicula vel.
                                                        Vestibulum porttitor nisi quis viverra hendrerit. Suspendisse vel volutpat nibh, vel 
                                                        elementum lacus. Maecenas commodo pulvinar dui, at cursus metus imperdiet vel.
                                                    </p>
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                        </div>
                   </div>
             </div>
            <div className="row">
                <div className="col">
                           <table className="h-100 w-100 text-white">
                                    <tbody>
                                        <tr>
                                             <td className="align-middle p-5">
                                                    <span className="fab fa-twitter p-3 fa-2x"></span>
                                                    <span className="fab fa-facebook p-3 fa-2x"></span>
                                                    <span className="fab fa-instagram p-3 fa-2x"></span>
                                                    <span className="fab fa-google-plus-g p-3 fa-2x"></span>
                                               <p>© Untitled. All rights reserved. Images Unsplash Design TEMPLATED</p>
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                </div>
            </div>
        </div>
    );
}
export default One;